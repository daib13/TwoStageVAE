import tensorflow as tf
import numpy as np
import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
import os
import sys
from network.flow_model import FlowModel
import argparse
import math
import time
import pickle
from fid_score import evaluate_fid_score
from dataset import load_dataset, load_test_dataset


def main():
    tf.reset_default_graph()
    if args.philly:
        exp_folder = args.output_path 
        model_path = args.output_path
    else:
        exp_folder = os.path.join(args.output_path, args.dataset, args.exp_name)
        if not os.path.exists(exp_folder):
            os.makedirs(exp_folder)
        model_path = exp_folder

    # dataset
    x, side_length, channels = load_dataset(args.dataset, args.root_folder)
    input_x = tf.placeholder(tf.uint8, [args.batch_size, side_length, side_length, channels], 'x')
    num_sample = np.shape(x)[0]
    print('Num Sample = {}.'.format(num_sample))

    # model
    model = FlowModel(input_x, args.latent_dim, args.num_flows)
    sess = tf.InteractiveSession()
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()

    if not args.val:
        writer = tf.summary.FileWriter(exp_folder, sess.graph)

        iteration_per_epoch = math.floor(float(num_sample) / float(args.batch_size))
        for epoch in range(args.epochs):
            np.random.shuffle(x)
            lr = args.lr if args.lr_epochs <= 0 else args.lr * math.pow(args.lr_fac, math.floor(float(epoch) / float(args.lr_epochs)))
            epoch_loss = 0
            for j in range(iteration_per_epoch):
                image_batch = x[j*args.batch_size:(j+1)*args.batch_size]
                loss = model.step(image_batch, lr, sess, writer, args.write_iteration)
                epoch_loss += loss 
            epoch_loss /= iteration_per_epoch

            print('Date: {date}\t'
                  'Epoch: [{0}/{1}]\t'
                  'Loss: {2:.4f}.'.format(epoch, args.epochs, epoch_loss, date=time.strftime('%Y-%m-%d %H:%M:%S')))
        saver.save(sess, os.path.join(model_path, 'model'))
    else:
        saver.restore(sess, os.path.join(model_path, 'model'))

    # test dataset 
    x, side_length, channels = load_test_dataset(args.dataset, args.root_folder)
    np.random.shuffle(x)
    x = x[0:10000]

    # reconstruction and generation
    img_recons = model.reconstruct(sess, x)
    img_gens = model.generate(sess, 10000)

    img_recons_sample = stich_imgs(img_recons)
    img_gens_sample = stich_imgs(img_gens)
    plt.imsave(os.path.join(exp_folder, 'recon_sample.jpg'), img_recons_sample)
    plt.imsave(os.path.join(exp_folder, 'gen_sample.jpg'), img_gens_sample)

    tf.reset_default_graph()
    fid_recon = evaluate_fid_score(img_recons.copy(), args.dataset, args.root_folder, True)
    fid_gen = evaluate_fid_score(img_gens.copy(), args.dataset, args.root_folder, True)
    print('Reconstruction Results:')
    print('FID = {:.4F}\n'.format(fid_recon))
    print('Generation Results:')
    print('FID = {:.4f}\n'.format(fid_gen))

    fid = open(os.path.join(exp_folder, 'report_fid.txt'), 'wt')
    fid.write('Reconstruction FID = {:.4f}\n'.format(fid_recon))
    fid.write('Generation FID = {:.4f}\n'.format(fid_gen))
    fid.close()


def stich_imgs(x, img_per_row=10, img_per_col=10):
    x_shape = np.shape(x)
    assert(len(x_shape) == 4)
    output = np.zeros([img_per_row*x_shape[1], img_per_col*x_shape[2], x_shape[3]])
    idx = 0
    for r in range(img_per_row):
        start_row = r * x_shape[1]
        end_row = start_row + x_shape[1]
        for c in range(img_per_col):
            start_col = c * x_shape[2]
            end_col = start_col + x_shape[2]
            output[start_row:end_row, start_col:end_col] = x[idx]
            idx += 1
            if idx == x_shape[0]:
                break
        if idx == x_shape[0]:
            break
    return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--root-folder', type=str, default='.')
    parser.add_argument('--output-path', type=str, default='./experiments')
    parser.add_argument('--exp-name', type=str, default='debug')

    parser.add_argument('--latent-dim', type=int, default=64)
    parser.add_argument('--dataset', type=str, default='mnist')
    parser.add_argument('--gpu', type=str, default='0')
    parser.add_argument('--num-flows', type=int, default=10)
    parser.add_argument('--batch-size', type=int, default=64)
    parser.add_argument('--epochs', type=int, default=400)
    parser.add_argument('--lr', type=float, default=1e-4)
    parser.add_argument('--lr-fac', type=float, default=0.5)
    parser.add_argument('--lr-epochs', type=int, default=150)
    parser.add_argument('--write-iteration', type=int, default=100)
    parser.add_argument('--val', default=False, action='store_true')

    parser.add_argument('--philly', default=False, action='store_true')

    args = parser.parse_args()
    print(args)

    os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
    os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)

    main()
