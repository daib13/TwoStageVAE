import tensorflow as tf 
import numpy as np 
from network.util import lrelu, conv2d, batch_norm, deconv2d
import network.util as ut 


class WAE:
    def __init__(self, x, latent_dim=64, loss_type='gan', wae_lambda=100.0, cost='l2'):
        self.raw_x = x
        self.x = (tf.cast(x, tf.float32) + 0.5) / 256.0 
        self.batch_size = int(self.x.get_shape().as_list()[0])
        self.latent_dim = latent_dim 
        self.is_training = tf.placeholder(tf.bool, [], 'is_training')
        self.loss_type = loss_type
        self.cost = cost
        self.wae_lambda = wae_lambda
        
        self.build_encoder()
        self.noise = tf.random_normal([self.batch_size, self.latent_dim])

        self.build_decoder()

        # -- Objectives, losses, penalties
        self.loss_gan, self.penalty = self.matching_penalty()
        self.loss_reconstruct = self.reconstruction_loss(self.x, self.x_hat)
        self.wae_objective = self.loss_reconstruct + self.wae_lambda * self.penalty
        self.loss_pretrain = self.pretrain_loss()

        self.summary = []
        self.summary.append(tf.summary.image('raw', self.x))
        self.summary.append(tf.summary.scalar('recon', self.loss_reconstruct))
        self.summary.append(tf.summary.scalar('penalty', self.penalty))
        self.summary.append(tf.summary.image('recon', self.x_hat))
        if self.loss_gan is not None:
            self.summary.append(tf.summary.scalar('gan', self.loss_gan))
        self.summary = tf.summary.merge(self.summary)

        self.pretrain_summary = self.loss_pretrain
        self.generate_summary = tf.summary.image('gen', self.x_hat)

        # optimizer
        self.add_optimizers()

    def build_encoder(self):
        with tf.variable_scope('encoder'):
            y = self.x 
            y = lrelu(conv2d(y, 64, 4, 4, 2, 2, name='conv1', use_sn=True))

            y = conv2d(y, 128, 4, 4, 2, 2, name='conv2', use_sn=True)
            y = batch_norm(y, is_training=self.is_training, scope='bn2')
            y = lrelu(y)

            y = tf.reshape(y, [self.x.get_shape().as_list()[0], -1])
            y = ut.linear(y, 1024, scope="fc3", use_sn=True)
            y = batch_norm(y, is_training=self.is_training, scope='bn3')
            y = lrelu(y)

            self.z = ut.linear(y, self.latent_dim, scope="en4", use_sn=True)

    def build_decoder(self):
        with tf.variable_scope('decoder'):
            y = self.z 
            final_side_length = self.x.get_shape().as_list()[1]
            data_depth = self.x.get_shape().as_list()[-1]

            y = tf.nn.relu(batch_norm(ut.linear(y, 1024, 'fc1'), is_training=self.is_training, scope='bn1'))
            y = tf.nn.relu(batch_norm(ut.linear(y, 128 * (final_side_length // 4) * (final_side_length // 4), scope='fc2'), is_training=self.is_training, scope='bn2'))
            y = tf.reshape(y, [self.batch_size, final_side_length // 4, final_side_length // 4, 128])
            y = tf.nn.relu(batch_norm(deconv2d(y, [self.batch_size, final_side_length // 2, final_side_length // 2, 64], 4, 4, 2, 2, name='conv3'), is_training=self.is_training, scope='bn3'))
            self.x_hat = tf.nn.sigmoid(deconv2d(y, [self.batch_size, final_side_length, final_side_length, data_depth], 4, 4, 2, 2, name='conv4'))

            self.loggamma_x = tf.get_variable('loggamma_x', [], tf.float32, tf.zeros_initializer())
            self.gamma_x = tf.exp(self.loggamma_x)

    def pretrain_loss(self):
        # Adding ops to pretrain the encoder so that mean and covariance
        # of Qz will try to match those of Pz
        mean_pz = tf.reduce_mean(self.noise, axis=0, keep_dims=True)
        mean_qz = tf.reduce_mean(self.z, axis=0, keep_dims=True)
        mean_loss = tf.reduce_mean(tf.square(mean_pz - mean_qz))
        cov_pz = tf.matmul(self.noise - mean_pz, self.noise - mean_pz, transpose_a=True)
        cov_pz /= self.batch_size
        cov_qz = tf.matmul(self.z - mean_qz, self.z - mean_qz, transpose_a=True)
        cov_qz /= self.batch_size
        cov_loss = tf.reduce_mean(tf.square(cov_pz - cov_qz))
        return mean_loss + cov_loss

    def matching_penalty(self):
        sample_qz = self.z
        sample_pz = tf.random_normal(self.z.get_shape().as_list())
        if self.loss_type == 'gan':
            loss_gan, loss_match = self.gan_penalty(sample_qz, sample_pz)
        elif self.loss_type == 'mmd':
            loss_gan, loss_match = self.mmd_penalty(sample_qz, sample_pz)
        else:
            assert False, 'Unknown penalty %s' % self.loss_type
        return loss_gan, loss_match

    def mmd_penalty(self, sample_qz, sample_pz):
        sigma2_p = 1.0
        kernel = 'IMQ'
        n = sample_pz.get_shape()[0]
        n = tf.cast(n, tf.int32)
        nf = tf.cast(n, tf.float32)
        half_size = (n * n - n) / 2

        norms_pz = tf.reduce_sum(tf.square(sample_pz), axis=1, keep_dims=True)
        dotprods_pz = tf.matmul(sample_pz, sample_pz, transpose_b=True)
        distances_pz = norms_pz + tf.transpose(norms_pz) - 2. * dotprods_pz

        norms_qz = tf.reduce_sum(tf.square(sample_qz), axis=1, keep_dims=True)
        dotprods_qz = tf.matmul(sample_qz, sample_qz, transpose_b=True)
        distances_qz = norms_qz + tf.transpose(norms_qz) - 2. * dotprods_qz

        dotprods = tf.matmul(sample_qz, sample_pz, transpose_b=True)
        distances = norms_qz + tf.transpose(norms_pz) - 2. * dotprods

        if kernel == 'RBF':
            # Median heuristic for the sigma^2 of Gaussian kernel
            sigma2_k = tf.nn.top_k(tf.reshape(distances, [-1]), half_size).values[half_size - 1]
            sigma2_k += tf.nn.top_k(tf.reshape(distances_qz, [-1]), half_size).values[half_size - 1]
            
            res1 = tf.exp( - distances_qz / 2. / sigma2_k)
            res1 += tf.exp( - distances_pz / 2. / sigma2_k)
            res1 = tf.multiply(res1, 1. - tf.eye(n))
            res1 = tf.reduce_sum(res1) / (nf * nf - nf)
            res2 = tf.exp( - distances / 2. / sigma2_k)
            res2 = tf.reduce_sum(res2) * 2. / (nf * nf)
            stat = res1 - res2
        elif kernel == 'IMQ':
            Cbase = 2. * self.latent_dim * sigma2_p
            stat = 0.
            for scale in [.1, .2, .5, 1., 2., 5., 10.]:
                C = Cbase * scale
                res1 = C / (C + distances_qz)
                res1 += C / (C + distances_pz)
                res1 = tf.multiply(res1, 1. - tf.eye(n))
                res1 = tf.reduce_sum(res1) / (nf * nf - nf)
                res2 = C / (C + distances)
                res2 = tf.reduce_sum(res2) * 2. / (nf * nf)
                stat += res1 - res2
        return None, stat

    def gan_penalty(self, sample_qz, sample_pz):
        # Pz = Qz test based on GAN in the Z space
        logits_Pz = z_adversary(sample_pz, self.latent_dim)
        logits_Qz = z_adversary(sample_qz, self.latent_dim, reuse=True)
        loss_Pz = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(
                logits=logits_Pz, labels=tf.ones_like(logits_Pz)))
        loss_Qz = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(
                logits=logits_Qz, labels=tf.zeros_like(logits_Qz)))
        loss_Qz_trick = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(
                logits=logits_Qz, labels=tf.ones_like(logits_Qz)))
        loss_adversary = self.wae_lambda * (loss_Pz + loss_Qz)
        # Non-saturating loss trick
        loss_match = loss_Qz_trick
        return loss_adversary, loss_match

    def reconstruction_loss(self, real, reconstr):
        # real = self.sample_points
        # reconstr = self.reconstructed
        if self.cost == 'l2':
            # c(x,y) = ||x - y||_2
            loss = tf.reduce_sum(tf.square(real - reconstr), axis=[1, 2, 3])
            loss = 0.2 * tf.reduce_mean(tf.sqrt(1e-08 + loss))
        elif self.cost == 'l2sq':
            # c(x,y) = ||x - y||_2^2
            loss = tf.reduce_sum(tf.square(real - reconstr), axis=[1, 2, 3])
            loss = 0.05 * tf.reduce_mean(loss)
        elif self.cost == 'l1':
            # c(x,y) = ||x - y||_1
            loss = tf.reduce_sum(tf.abs(real - reconstr), axis=[1, 2, 3])
            loss = 0.02 * tf.reduce_mean(loss)
        else:
            assert False, 'Unknown cost function %s' % opts['cost']
        return loss

    def add_optimizers(self):
        self.lr = tf.placeholder(tf.float32, [], 'lr')
        all_variables = tf.all_variables()
        z_adv_vars = [var for var in all_variables if 'z_adversary' in var.name]
        encoder_vars = [var for var in all_variables if 'encoder' in var.name]
        decoder_vars = [var for var in all_variables if 'decoder' in var.name]
        ae_vars = encoder_vars + decoder_vars

        # Auto-encoder optimizer
        self.ae_opt = tf.train.AdamOptimizer(self.lr, beta1=0.5).minimize(loss=self.wae_objective, var_list=ae_vars)

        # Discriminator optimizer for WAE-GAN
        if self.loss_type == 'gan':
            self.z_adv_opt = tf.train.AdamOptimizer(self.lr, beta1=0.5).minimize(loss=self.loss_gan, var_list=z_adv_vars)
        else:
            self.z_adv_opt = None

        # Encoder optimizer
        self.pretrain_opt = tf.train.AdamOptimizer(self.lr, beta1=0.5).minimize(loss=self.loss_pretrain, var_list=encoder_vars)

    def pretrain_encoder(self, x, lr, sess):  
        np.random.shuffle(x)
        iteration_per_epoch = int(np.shape(x)[0] // self.batch_size)
        for i in range(iteration_per_epoch):
            x_batch = x[i*self.batch_size:(i+1)*self.batch_size]
            loss, _ =  sess.run([self.loss_pretrain, self.pretrain_opt], feed_dict={self.raw_x: x_batch, self.lr: lr, self.is_training: True})

    def train(self, x, lr, sess):
        np.random.shuffle(x)
        iteration_per_epoch = int(np.shape(x)[0] // self.batch_size)
        total_loss = 0
        for i in range(iteration_per_epoch):
            x_batch = x[i*self.batch_size:(i+1)*self.batch_size]
            loss, _, summary = sess.run([self.wae_objective, self.ae_opt, self.summary], feed_dict={self.raw_x: x_batch, self.lr: lr, self.is_training: True})
            total_loss += loss 

            if self.loss_type == 'gan':
                _ = sess.run([self.loss_gan, self.z_adv_opt], feed_dict={self.raw_x: x_batch, self.lr: lr, self.is_training: True})
        total_loss /= iteration_per_epoch
        return total_loss, summary


def z_adversary(inputs, z_dim, reuse=False):
    num_units = 4
    num_layers = 1024
    nowozin_trick = True
    # No convolutions as GAN happens in the latent space
    with tf.variable_scope('z_adversary', reuse=reuse):
        hi = inputs
        for i in range(num_layers):
            hi = linear(hi, num_units, scope='h%d_lin' % (i + 1))
            hi = tf.nn.relu(hi)
        hi = linear(hi, 1, scope='hfinal_lin')
        if nowozin_trick:
            # We are doing GAN between our model Qz and the true Pz.
            # Imagine we know analytical form of the true Pz.
            # The optimal discriminator for D_JS(Pz, Qz) is given by:
            # Dopt(x) = log dPz(x) - log dQz(x)
            # And we know exactly dPz(x). So add log dPz(x) explicitly 
            # to the discriminator and let it learn only the remaining
            # dQz(x) term. This appeared in the AVB paper.
            sigma2_p = 1.0
            normsq = tf.reduce_sum(tf.square(inputs), 1)
            hi = hi - normsq / 2. / sigma2_p - 0.5 * tf.log(2. * np.pi) - 0.5 * z_dim * np.log(sigma2_p)
    return hi


def linear(input_, output_dim, scope=None, init='normal', reuse=None):
    """Fully connected linear layer.

    Args:
        input_: [num_points, ...] tensor, where every point can have an
            arbitrary shape. In case points are more than 1 dimensional,
            we will stretch them out in [numpoints, prod(dims)].
        output_dim: number of features for the output. I.e., the second
            dimensionality of the matrix W.
    """

    stddev = 0.0099999
    bias_start = 0.0
    shape = input_.get_shape().as_list()

    assert len(shape) > 0
    in_shape = shape[1]
    if len(shape) > 2:
        # This means points contained in input_ have more than one
        # dimensions. In this case we first stretch them in one
        # dimensional vectors
        input_ = tf.reshape(input_, [-1, np.prod(shape[1:])])
        in_shape = np.prod(shape[1:])

    with tf.variable_scope(scope or "lin", reuse=reuse):
        if init == 'normal':
            matrix = tf.get_variable(
                "W", [in_shape, output_dim], tf.float32,
                tf.random_normal_initializer(stddev=stddev))
        else:
            matrix = tf.get_variable(
                "W", [in_shape, output_dim], tf.float32,
                tf.constant_initializer(np.identity(in_shape)))
        bias = tf.get_variable(
            "b", [output_dim],
            initializer=tf.constant_initializer(bias_start))

    return tf.matmul(input_, matrix) + bias