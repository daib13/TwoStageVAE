import tensorflow as tf 
import math 
import numpy as np 
from tensorflow.python.training.moving_averages import assign_moving_average
from network.util import *


def xavier_init(size):
    in_dim = size[0]
    xavier_stddev = 1. / tf.sqrt(in_dim / 2.)
    return tf.random_normal(shape=size, stddev=xavier_stddev)


class FlowModel(object):
    def __init__(self, x, latent_dim, num_flows):
        self.raw_x = x 
        self.x = (tf.cast(self.raw_x, tf.float32) + 0.5) / 256.0 
        self.batch_size = x.get_shape().as_list()[0]
        self.latent_dim = latent_dim
        self.num_flows = num_flows

        self.is_training = tf.placeholder(tf.bool, [])

        self.__build_network()
        self.__build_loss()
        self.__build_summary()
        self.__build_optimizer()

    def __build_network(self):
        self.build_encoder()
        self.build_flow()
        self.build_decoder()

    def build_encoder(self):
        with tf.variable_scope('encoder'):
            y = self.x 
            y = lrelu(conv2d(y, 64, 4, 4, 2, 2, name='conv1', use_sn=True))

            y = conv2d(y, 128, 4, 4, 2, 2, name='conv2', use_sn=True)
            y = batch_norm(y, is_training=self.is_training, scope='bn2')
            y = lrelu(y)

            y = tf.reshape(y, [self.x.get_shape().as_list()[0], -1])
            y = linear(y, 1024, scope="fc3", use_sn=True)
            y = batch_norm(y, is_training=self.is_training, scope='bn3')
            y = lrelu(y)

            gaussian_params = linear(y, 2 * self.latent_dim, scope="en4", use_sn=True)
            self.mu_z = gaussian_params[:, :self.latent_dim]
            self.sd_z = 1e-6 + tf.nn.softplus(gaussian_params[:, self.latent_dim:])
            self.logsd_z = tf.log(self.sd_z)
            self.z = self.mu_z + tf.random_normal([self.batch_size, self.latent_dim]) * self.sd_z

    def build_flow(self):
        with tf.variable_scope('flow'):
            self.logdet_jacobian = 0.0
            u, w, b, uw, muw, u_hat, zwb, f_z, psi, psi_u = [], [], [], [], [], [], [], [], [], []
            for i in range(self.num_flows):
                u.append(tf.Variable(xavier_init([self.latent_dim, 1]), name=("U_"+str(i))))
                w.append(tf.Variable(xavier_init([self.latent_dim, 1]), name=("V_"+str(i))))
                b.append(tf.Variable(xavier_init([1, 1]))) #scalar
                uw.append(tf.matmul(tf.transpose(w[i]), u[i]))
    
                muw.append(-1 + tf.nn.softplus(uw[i])) # = -1 + T.log(1 + T.exp(uw))
                u_hat.append(u[i] + (muw[i] - uw[i]) * w[i] / tf.reduce_sum(tf.matmul(tf.transpose(w[i]),w[i])))
                if(i==0):
                    zwb.append(tf.matmul(self.z,w[i]) + b[i])
                    f_z.append(self.z + tf.multiply( tf.transpose(u_hat[i]), tf.tanh(zwb[i])))
                else:
                    zwb.append(tf.matmul(f_z[i-1],w[i]) + b[i])
                    f_z.append(f_z[i-1] + tf.multiply( tf.transpose(u_hat[i]), tf.tanh(zwb[i])))

                psi.append(tf.matmul(w[i],tf.transpose(1-tf.multiply(tf.tanh(zwb[i]), tf.tanh(zwb[i]))))) # tanh(x)dx = 1 - tanh(x)**2
                psi_u.append(tf.matmul(tf.transpose(psi[i]), u_hat[i]))
                self.logdet_jacobian += tf.log(tf.abs(1 + psi_u[i]))

            if len(f_z) != 0:
                self.z = f_z[-1]

    def build_decoder(self):
        with tf.variable_scope('decoder'):
            y = self.z 
            final_side_length = self.x.get_shape().as_list()[1]
            data_depth = self.x.get_shape().as_list()[-1]

            y = tf.nn.relu(batch_norm(linear(y, 1024, 'fc1'), is_training=self.is_training, scope='bn1'))
            y = tf.nn.relu(batch_norm(linear(y, 128 * (final_side_length // 4) * (final_side_length // 4), scope='fc2'), is_training=self.is_training, scope='bn2'))
            y = tf.reshape(y, [self.batch_size, final_side_length // 4, final_side_length // 4, 128])
            y = tf.nn.relu(batch_norm(deconv2d(y, [self.batch_size, final_side_length // 2, final_side_length // 2, 64], 4, 4, 2, 2, name='conv3'), is_training=self.is_training, scope='bn3'))
            self.x_hat = tf.nn.sigmoid(deconv2d(y, [self.batch_size, final_side_length, final_side_length, data_depth], 4, 4, 2, 2, name='conv4'))

            self.loggamma_x = tf.get_variable('loggamma_x', [], tf.float32, tf.zeros_initializer())
            self.gamma_x = tf.exp(self.loggamma_x)

    def __build_loss(self):
        HALF_LOG_TWO_PI = 0.91893

        self.gen_loss = tf.reduce_sum(tf.square((self.x - self.x_hat) / self.gamma_x) / 2.0 + self.loggamma_x + HALF_LOG_TWO_PI) / float(self.batch_size)
        self.kl_loss = (tf.reduce_sum(tf.square(self.sd_z) + tf.square(self.mu_z) - 1.0 - 2*self.logsd_z) / 2.0 - tf.reduce_sum(self.logdet_jacobian)) / float(self.batch_size) 
        self.loss = self.gen_loss + self.kl_loss

    def __build_summary(self):
        with tf.name_scope('summary'):
            self.summary = []
            self.summary.append(tf.summary.image('input', self.x))
            self.summary.append(tf.summary.image('recon', self.x_hat))
            self.summary.append(tf.summary.scalar('kl_loss', self.kl_loss))
            self.summary.append(tf.summary.scalar('gen_loss', self.gen_loss))
            self.summary.append(tf.summary.scalar('loss', self.loss))
            self.summary = tf.summary.merge(self.summary)

    def __build_optimizer(self):
        self.lr = tf.placeholder(tf.float32, [], 'lr')
        self.global_step = tf.get_variable('global_step', [], tf.int32, tf.zeros_initializer(), trainable=False)
        self.opt = tf.train.AdamOptimizer(self.lr).minimize(self.loss, self.global_step)

    def step(self, input_batch, lr, sess, writer=None, write_iteration=600):
        loss, summary, _ = sess.run([self.loss, self.summary, self.opt], feed_dict={self.raw_x: input_batch, self.lr: lr, self.is_training: True})
        global_step = self.global_step.eval(sess)
        if global_step % write_iteration == 0 and writer is not None:
            writer.add_summary(summary, global_step)
        return loss 

    def reconstruct(self, sess, x):
        num_sample = np.shape(x)[0]
        num_iter = math.ceil(float(num_sample) / float(self.batch_size))
        x_extend = np.concatenate([x, x[0:self.batch_size]], 0)
        recon_x = []
        for i in range(num_iter):
            recon_x_batch = sess.run(self.x_hat, feed_dict={self.raw_x: x_extend[i*self.batch_size:(i+1)*self.batch_size], self.is_training: False})
            recon_x.append(recon_x_batch)
        recon_x = np.concatenate(recon_x, 0)[0:num_sample]
        return recon_x 

    def generate(self, sess, num_sample):
        num_iter = math.ceil(float(num_sample) / float(self.batch_size))
        gen_samples = []
        for i in range(num_iter):
            z = np.random.normal(0, 1, [self.batch_size, self.latent_dim])
            x = sess.run(self.x_hat, feed_dict={self.z: z, self.is_training: False})
            gen_samples.append(x)
        gen_samples = np.concatenate(gen_samples, 0)
        return gen_samples[0:num_sample]